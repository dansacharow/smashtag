﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GridSpace : MonoBehaviour {

    public Button button;
    public Text buttonText;
    private Gamecontroller gameController;  // a reference to Gamecontroller

    public void SetSpace() {

        buttonText.text = gameController.getPlayerSide();
        button.interactable = false;
        gameController.endTurn();


    }

    public void setGameControllerReference(Gamecontroller controller) {

        gameController = controller;

    }


}
