﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Gamecontroller : MonoBehaviour {

    //to do: add a scoring system, suggested from Ryth!!!

    public Text[] buttonList;
    private string playerSide;
    public GameObject gameOverPanel;
    public Text gameOverText;
    private int moveCount;
    private bool fullWin = false;
    public GameObject restartButton;

    void Awake() {

        gameOverPanel.SetActive(false);
        restartButton.SetActive(false);
        setGameControllerReferenceOnButtons();
        playerSide = "1";
        moveCount = 0;

    }

    void setGameControllerReferenceOnButtons() {

        for (int i = 0; i < buttonList.Length; i++) {

            buttonList[i].GetComponentInParent<GridSpace>().setGameControllerReference(this);
        }

    }

    public string getPlayerSide() { 

        return playerSide;

    }

    public void endTurn() {
        //rows

        moveCount++;

        if (buttonList[0].text == playerSide && buttonList[1].text == playerSide && buttonList[2].text == playerSide) {
            fullWin = true;
            gameOver();
        }

        if (buttonList[3].text == playerSide && buttonList[4].text == playerSide && buttonList[5].text == playerSide)
        {
            fullWin = true;
            gameOver();
        }

        if (buttonList[6].text == playerSide && buttonList[7].text == playerSide && buttonList[8].text == playerSide)
        {
            fullWin = true;
            gameOver();
        }

        //columns

        if (buttonList[0].text == playerSide && buttonList[3].text == playerSide && buttonList[6].text == playerSide)
        {
            fullWin = true;
            gameOver();
        }

        if (buttonList[1].text == playerSide && buttonList[4].text == playerSide && buttonList[7].text == playerSide)
        {
            fullWin = true;
            gameOver();
        }

        if (buttonList[2].text == playerSide && buttonList[5].text == playerSide && buttonList[8].text == playerSide)
        {
            fullWin = true;
            gameOver();
        }

        //two diagonals

        if (buttonList[0].text == playerSide && buttonList[4].text == playerSide && buttonList[8].text == playerSide)
        {
            fullWin = true;
            gameOver();
        }

        if (buttonList[2].text == playerSide && buttonList[4].text == playerSide && buttonList[6].text == playerSide)
        {
            fullWin = true;
            gameOver();
        }

        if (moveCount >= 9) {
            if (fullWin == false)
            {
                setGameOverText("Stalemate!");
                restartButton.SetActive(true);
            }
            else {

                gameOver();
            }
            
            
        }

        changeSides();
    }

    void gameOver() {

        setBoardInteractable(false);
        setGameOverText(playerSide + " Wins!");
        restartButton.SetActive(true);

    }

    void changeSides() {

        playerSide = (playerSide == "1") ? "0" : "1";

        
    }

    void setGameOverText(string value) {
        gameOverPanel.SetActive(true);
        gameOverText.text = value;
    }

    public void restartGame() {
        //reset everything to default start up values
        playerSide = "1";
        moveCount = 0;
        gameOverPanel.SetActive(false);
        restartButton.SetActive(false);
        fullWin = false;

        setBoardInteractable(true);

        for (int i = 0; i < buttonList.Length; i++)
        {
            buttonList[i].text = "";
        }


    }

    void setBoardInteractable(bool toggle) {

        for (int i = 0; i < buttonList.Length; i++)
        {
            buttonList[i].GetComponentInParent<Button>().interactable = toggle;
           
        }
        

    }

}
